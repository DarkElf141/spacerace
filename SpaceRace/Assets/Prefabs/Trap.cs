﻿using SpaceRace.BaseClasses;
using System;
using System.Windows.Media.Media3D;

namespace SpaceRace.Assets.Prefabs
{
    public class Trap : GameObject
    {
        public Trap(Point3D position)
        {
            Size = new(1, (double)new Random().Next(6, 11) / 10, 1);
            Position = new(position.X,  Size.Y / 2, position.Z);
            Mesh = BaseClasses.Meshes.Cube();
            Name = "Trap";
            Collider = new() { GameObject = this };
        }
        
    }
}
