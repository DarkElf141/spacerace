﻿using System.Windows.Media.Media3D;
using System.Windows.Media;
using System;
using System.Windows.Media.Imaging;
using SpaceRace.BaseClasses;
using SpaceRace.Assets.Scripts;
using System.Windows;

namespace SpaceRace.Assets.Scenes
{
    public class TestScene : Scene
    {
        public TestScene()
        {
            Platform.Collider = new() { GameObject = Platform };
            Player.Collider = new() { GameObject = Player };
            Children.Add(AmbientLight);
            Children.Add(SkyBox);
            Children.Add(Platform);
            Children.Add(Player);

            Player.PositionChanged += CameraPosition;

            Load();
        }

        GameObject Platform = new()
        {
            Position = new(0, -0.05, 400),
            Size = new(10, 0.1, 1000),
            Material = new DiffuseMaterial(new SolidColorBrush(Color.FromArgb(50, 10, 10, 10))),
            Mesh = Meshes.Cube(),
            Name = "Platform",
            Scripts = new()
                {
                    new PlatformEffects(),
                }
        };


        GameObject SkyBox = new()
        {
            Position = new(0, 1, 0),
            Size = new(100000, 100000, 100000),
            BackMaterial = new MaterialGroup()
            {
                Children = new()
                {
                    new DiffuseMaterial(new ImageBrush(new BitmapImage(new Uri(@"pack://application:,,,/Assets/Images/5.jpg")))),
                }
            },
            Mesh = Meshes.Sphere(),
            Name = "SkyBox",
        };

        public GameObject Player { get; set; } = new()
        {
            Position = new(0, 0.2, 0),
            Size = new(0.4, 0.4, 0.5),
            Material = new DiffuseMaterial(new SolidColorBrush(Color.FromArgb(100, 0, 255, 255))),
            IsGravity = true,
            Mesh = Meshes.Cube(),
            Name = "Player",
            Scripts = new()
                {
                    new PlayerControl(),
                    new TrapsGenerator(),
                    new ColorChanger(),
                }
        };

        GameObject AmbientLight = new()
        {
            Content = new AmbientLight()
            {
                Color = Color.FromRgb(100, 100, 100),
            }
        };



        private void CameraPosition(object? sender, Point3D e)
        {
            if (Camera is PerspectiveCamera camera && sender is GameObject gameObject)
            {
                camera.Position = e + gameObject.GetVector(TranslateSide.Up) * 1.5 + gameObject.GetVector(TranslateSide.Back) * 10;
                camera.LookDirection = Vector3D.Subtract((Vector3D)e, (Vector3D)camera.Position);
            }
        }

    }
}
