﻿using System.Windows.Media.Media3D;

namespace SpaceRace.BaseClasses
{
    public class BoxCollider
    {
        public GameObject GameObject { private get; set; }
        public Rect3D Bounds { get => new(GameObject.Position - Vector3D.Divide(GameObject.Size, 2), (Size3D)GameObject.Size); }
    }
}
