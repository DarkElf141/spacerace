﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Media3D;

namespace SpaceRace.BaseClasses
{
    public enum TranslateSide
    {
        Forward = 1,
        Back,
        Up,
        Down,
        Left,
        Right,
    }
    public class GameObject : ModelVisual3D
    {
        /// <summary>
        /// Properties 
        /// </summary>

        public static readonly DependencyProperty PositionProperty = DependencyProperty.Register(
            "Position", typeof(Point3D), typeof(GameObject), new PropertyMetadata(new Point3D(0,0,0)));

        private MeshGeometry3D mesh = new();
        private Material material = new DiffuseMaterial(Brushes.White);
        private Material backMaterial;
        private List<Script> scripts = new();

        public bool IsGravity { get; set; } = false;
        public bool IsFallingNow { get; set; } = false;

        public string Name { get; set; } = "GameObject";

        public BoxCollider? Collider { get; set; }
        public Scene Scene { get; set; }

        public MeshGeometry3D Mesh
        {
            get => mesh;
            set
            {
                mesh = value;
                SetModel();
            }
        }
        public Material Material
        {
            get => material;
            set
            {
                material = value;
                SetModel();
            }
        }
        public Material BackMaterial
        {
            get => backMaterial;
            set
            {
                backMaterial = value;
                SetModel();
            }
        }
        public Vector3D Size
        {
            get => new(Transform.Value.M11, Transform.Value.M22, Transform.Value.M33);
            set
            {
                Matrix3D matrix = GetMatrix();
                matrix.ScaleAt(new(1 / Size.X, 1 / Size.Y, 1 / Size.Z), Position);
                matrix.ScaleAt(value, Position);
                SetMatrix(matrix);

                SizeChanged?.Invoke(this, value);
            }
        }
        public Point3D Position
        {
            get => (Point3D)GetValue(PositionProperty);
            set
            {
                Matrix3D matrix = GetMatrix();
                matrix.Translate(new(value.X - Position.X, value.Y - Position.Y, value.Z - Position.Z));
                SetMatrix(matrix);

                SetValue(PositionProperty, value);

                PositionChanged?.Invoke(this, value);
            }
        }
        public List<Script> Scripts
        {
            get => scripts;
            set
            {
                scripts = value;
                scripts.ForEach(script => script.GameObject ??= this);
            }
        }

        /// <summary>
        /// Constructors
        /// </summary>
        public GameObject() => SetModel();
        public void SetModel() => Content = new GeometryModel3D(Mesh, Material) { BackMaterial = BackMaterial };

        /// <summary>
        /// Get/Set Matrix3D
        /// </summary>
        private Matrix3D GetMatrix() => Transform.Value;
        private void SetMatrix(Matrix3D newMatrix) => Transform = new MatrixTransform3D(newMatrix);


        ///<summary>
        /// Translate methods
        ///</summary>
        public void Translate(TranslateSide translateSide, double speedTranslate)
            => Position += GetVector(translateSide) * speedTranslate;
        public void Translate(Vector3D translateSide) => Position += translateSide;
        public Vector3D GetVector(TranslateSide translateSide) => translateSide switch
        {
            TranslateSide.Forward => new(Transform.Value.M31, Transform.Value.M32, 1),
            TranslateSide.Back => -GetVector(TranslateSide.Forward),

            TranslateSide.Up => new(Transform.Value.M21, 1, Transform.Value.M23),
            TranslateSide.Down => -GetVector(TranslateSide.Up),

            TranslateSide.Left => new(1, Transform.Value.M12, Transform.Value.M13),
            TranslateSide.Right => -GetVector(TranslateSide.Left),

            _ => throw new ArgumentException("Incorrect translete side"),
        };


        ///<summary>
        /// Rotate
        ///</summary>
        public Vector3D GetPitchAxis() => new(Transform.Value.M11, Transform.Value.M12, Transform.Value.M13);
        public Vector3D GetRollAxis() => new(Transform.Value.M21, Transform.Value.M22, Transform.Value.M23);
        public Vector3D GetYawAxis() => new(Transform.Value.M31, Transform.Value.M32, Transform.Value.M33);
        public void Rotate(Quaternion quaternion)
        {
            Matrix3D matrix = GetMatrix();
            matrix.RotateAt(quaternion, Position);
            SetMatrix(matrix);

            RotationChanged?.Invoke(this, EventArgs.Empty);
        }


        /// <summary>
        /// Property Changers
        /// </summary>
        public event EventHandler<Point3D>? PositionChanged;
        public event EventHandler<Vector3D>? SizeChanged;
        public event EventHandler? RotationChanged;

    }
}
