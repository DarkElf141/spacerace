﻿    using SpaceRace.BaseClasses;
using System.Windows.Media;
using System.Windows.Media.Media3D;
using Windows.ApplicationModel.Background;

namespace SpaceRace.Assets.Scripts
{
    public class ColorChanger : Script
    {
        public static Material ColorsMaterial = new DiffuseMaterial(Brushes.White);
        private readonly byte max = 230, min = 50, step = 5;
        private byte r, g, b;
        public override void Start()
        {
            r = max;
            g = min;
            b = min;
        }
        public override void Update()
        {
            if (r == max && g < max && b == min) g += step;
            else if (r > min && g == max && b == min) r -= step;
            else if (r == min && g == max && b < max) b += step;
            else if (r == min && g > min && b == max) g -= step;
            else if (r < max && g == min && b == max) r += step;
            else if (r == max && g == min && b > min) b -= step;

            ColorsMaterial = new MaterialGroup()
            {
                Children = new()
                    {
                        new DiffuseMaterial(new SolidColorBrush(Color.FromArgb(255,r,g,b))),
                        new EmissiveMaterial(new SolidColorBrush(Color.FromArgb(125,r,g,b))),
                    }
            };
        }
    }
}
