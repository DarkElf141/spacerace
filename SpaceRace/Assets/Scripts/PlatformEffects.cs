﻿using SpaceRace.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SpaceRace.Assets.Scripts
{
    public class PlatformEffects : Script
    {
        private readonly List<GameObject> stars = new(200);

        public override void Start()
        {
            Enumerable.Repeat(SetStar, 200).ToList().ForEach(x=>x.Invoke());
        }

        public override void Update()
        {
            for(int i = 0; i< stars.Count; i++)
            {
                stars[i].Translate(TranslateSide.Forward, stars[i].Size.Z * 100 * Scene.DeltaTime);

                if (stars[i].Position.Z > GameObject.Position.Z + 500)
                {
                    GameObject.Scene.Children.Remove(stars[i]);
                    stars.Remove(stars[i]);
                    SetStar();
                    i--;
                }
            }
        }

        private void SetStar( )
        {
             Random rand = new();
             GameObject star = new()
             {
                 Position = new(rand.Next(-5, 5) + rand.NextDouble(), -0.1 - rand.NextDouble(), GameObject.Position.Z - 20),
                 Size = new(0.02, 0.1, rand.Next(1, 10)),
                 Material = ColorChanger.ColorsMaterial,
                 Mesh = Meshes.Quad(),
             };

             stars.Add(star);
             GameObject.Scene.Instantiate(star);
        }
        
    }
}
