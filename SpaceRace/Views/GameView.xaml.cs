﻿using SpaceRace.Assets.Scenes;
using SpaceRace.BaseClasses;
using System;
using System.Collections.ObjectModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using Windows.System.RemoteSystems;

namespace SpaceRace.Views
{
    /// <summary>
    /// Interaction logic for GameView.xaml
    /// </summary>
    public partial class GameView : Page
    {
        public GameView(MainWindow window, string userName)
        {
            InitializeComponent();
            DataContext = this;
            Window = window;

            SaveData = Save.LoadGame();
            User =  SaveData.FirstOrDefault(x => x.Name == userName);

            if (User == null) {
                User = new(userName);
                SaveData.Add(User);
            }

            Mouse.OverrideCursor = Cursors.None;
            StartGame();
        }
        private MainWindow Window;

        public static readonly DependencyProperty SceneProperty = DependencyProperty.Register(
           "Scene", typeof(TestScene), typeof(GameView), new PropertyMetadata(null));
        public static readonly DependencyProperty TopVisibilityProperty = DependencyProperty.Register(
           "TopVisibility", typeof(Visibility), typeof(GameView), new PropertyMetadata(Visibility.Hidden));
        public static readonly DependencyProperty UserProperty = DependencyProperty.Register(
           "User", typeof(User), typeof(GameView), new PropertyMetadata(null));
        public static readonly DependencyProperty SaveDataProperty = DependencyProperty.Register(
           "SaveData", typeof(ObservableCollection<User>), typeof(GameView));

        public ObservableCollection<User> SaveData
        {
            get => (ObservableCollection<User>)GetValue(SaveDataProperty);
            set => SetValue(SaveDataProperty, value);
        }
        public User? User
        {
            get => (User?)GetValue(UserProperty);
            set => SetValue(UserProperty, value);
        }
        public TestScene Scene
        {
            get => (TestScene)GetValue(SceneProperty);
            set => SetValue(SceneProperty, value);
        }
        public Visibility TopVisibility
        {
            get => (Visibility)GetValue(TopVisibilityProperty);
            set => SetValue(TopVisibilityProperty, value);
        }

        private void StartGame()
        {
            Scene = new()
            {
                User = User,
                Width = Window.ActualWidth,
                Height = Window.ActualHeight,
            };
            Scene.IsEndGameChanged += Scene_IsEndGameChanged;
        }

        private async void Scene_IsEndGameChanged(object? sender, EventArgs e)
        {
            SaveData = new(SaveData.OrderByDescending(x => x.Score));
            Save.SaveGame(SaveData);
            
            TopVisibility = Visibility.Visible;            
            await Task.Delay(500);

            while (!Keyboard.IsKeyDown(Key.Space))
            {
                if (Keyboard.IsKeyDown(Key.Escape))
                {
                    Mouse.OverrideCursor = Cursors.Arrow;
                    Window.OpenMenu();
                    return;
                };
                await Task.Delay(5);
            }

            await Task.Delay(200);
            TopVisibility = Visibility.Hidden;

            StartGame();
        }



    }
}
