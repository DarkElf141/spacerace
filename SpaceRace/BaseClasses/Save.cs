﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace SpaceRace.BaseClasses
{
    public static class Save
    {
        public static void SaveGame (ObservableCollection<User> saveData)
        {
            if (!File.Exists("SaveData.xml"))
                File.Create("SaveData.xml").Close();

            XmlSerializer serializer = new(typeof(ObservableCollection<User>));

            using (FileStream textWriter = new("SaveData.xml", FileMode.OpenOrCreate))
                serializer.Serialize(textWriter, saveData);
        }
        public static ObservableCollection<User> LoadGame()
        {
            if (!File.Exists("SaveData.xml"))
            {
                File.Create("SaveData.xml").Close();
                return new();
            }

            XmlSerializer deserializer = new(typeof(ObservableCollection<User>));
            try
            {
                using (FileStream textReader = new("SaveData.xml", FileMode.OpenOrCreate))
                    return (ObservableCollection<User>)deserializer.Deserialize(textReader);
            }
            catch
            {
                File.Delete("SaveData.xml");
                return new();
            }

        }
    }

    [Serializable]
    public class User : ISerializable
    {
        public string Name { get; set; } = "No Name";
        public int Score { get; set; } = 0;

        public User() { }
        public User(string name) => Name = name;

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("Name", typeof(string));
            info.AddValue("Score", typeof(int));
        }
    }
}
