﻿using SpaceRace.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SpaceRace.Views
{
    /// <summary>
    /// Interaction logic for VerificationView.xaml
    /// </summary>
    public partial class VerificationView : Page
    {
        public VerificationView(MainWindow window)
        {
            InitializeComponent();
            DataContext = this;
            Window = window;
        }
        private MainWindow Window;
        public static DependencyProperty UserNameProperty = DependencyProperty.Register(
            "UserName", typeof(string), typeof(VerificationView), new PropertyMetadata(string.Empty));
        public string UserName
        {
            get => (string)GetValue(UserNameProperty);
            set => SetValue(UserNameProperty, value);
        }

        public ICommand StartGame
        {
            get => new RelayCommand(obj =>
            {
                if (obj is Button btn)
                {
                    btn.Visibility = Visibility.Hidden;
                    Window.StartGame(UserName);
                    btn.Visibility = Visibility.Visible;
                }
                
            }, obj => !string.IsNullOrWhiteSpace(UserName));
        }
        public ICommand CloseGame
        {
            get => new RelayCommand(obj =>
            {
                Window.Close();
            });
        }
    }
}
