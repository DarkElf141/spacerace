﻿using SpaceRace.BaseClasses;
using System;
using System.Linq;
using System.Windows.Input;


namespace SpaceRace.Assets.Scripts
{
    class PlayerControl : Script
    {
        GameObject SkyBox, Platform;
        private bool IsJumpActive = false;
        public override void Start()
        {
            SkyBox = GameObject.Scene.Children.OfType<GameObject>().First(item => item.Name == "SkyBox");
            Platform = GameObject.Scene.Children.OfType<GameObject>().First(item => item.Name == "Platform");
            SkyBox.Rotate(new(SkyBox.GetPitchAxis() + SkyBox.GetRollAxis(), new Random().Next(0, 361)));
        }
        public override void Update()
        {

            GameObject.Translate(TranslateSide.Forward, 30 * Scene.DeltaTime);
            if (Keyboard.IsKeyDown(Key.W)) GameObject.Translate(TranslateSide.Forward, 15 * Scene.DeltaTime);
            if (Keyboard.IsKeyDown(Key.A)) GameObject.Translate(TranslateSide.Left, 8 * Scene.DeltaTime);
            if (Keyboard.IsKeyDown(Key.D)) GameObject.Translate(TranslateSide.Right, 8 * Scene.DeltaTime);

            SkyBox.Rotate(new(SkyBox.GetPitchAxis() + SkyBox.GetRollAxis(), 0.01));
            SkyBox.Position = new(SkyBox.Position.X, SkyBox.Position.Y, GameObject.Position.Z);
            Platform.Position = new(Platform.Position.X, Platform.Position.Y, GameObject.Position.Z);

            if (GameObject.Position.Y < -5)
                GameObject.Scene.EndGame();

            if (Keyboard.IsKeyDown(Key.Space) && !IsJumpActive && !GameObject.IsFallingNow) IsJumpActive = true;

            if (IsJumpActive)
            {
                GameObject.Translate(GameObject.GetVector(TranslateSide.Up) * 20 * Scene.DeltaTime + GameObject.GetVector(TranslateSide.Forward) * 30 * Scene.DeltaTime);

                if (GameObject.Position.Y > 1.5)
                    IsJumpActive = false;
            }
        }
    }
}
