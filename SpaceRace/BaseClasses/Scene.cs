﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Diagnostics;
using System.Diagnostics.Eventing.Reader;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Media3D;
using System.Windows.Threading;

namespace SpaceRace.BaseClasses
{
    public class Scene : Viewport3D
    {
        public static readonly DependencyProperty FPSProperty = DependencyProperty.Register(
           "FPS", typeof(int), typeof(Scene));
        public int FPS
        {
            get => (int)GetValue(FPSProperty);
            set => SetValue(FPSProperty, value);
        }

        
        public static double DeltaTime = 0.016;
        public bool isEndGame = false;
        public User User { get; set; }
        
        public event EventHandler? IsEndGameChanged;
        public bool IsEndGame
        {
            get => isEndGame;
            set
            {
                isEndGame = value;
                IsEndGameChanged?.Invoke(this, EventArgs.Empty);
            }
        }
        
        public Scene()
        {
            Camera = new PerspectiveCamera() { FieldOfView = 60 };
        }

        protected async void Load()
        {
            Start();

            int frameCount = 0;
            Stopwatch stopwatch = new();
            stopwatch.Start();
            long startTime = stopwatch.ElapsedMilliseconds;
            long previourTime = stopwatch.ElapsedMilliseconds;
            while (true)
            {
                if (IsEndGame)
                {
                    stopwatch.Stop();
                    return;
                }
                Update();
                if (IsEndGame)
                {
                    stopwatch.Stop();
                    return;
                }

                long pastTime = (stopwatch.ElapsedMilliseconds - previourTime);
                if (pastTime < 1000.0 / 60)
                {
                    await Task.Delay((int)(1000.0 / 60 - pastTime));
                    DeltaTime = 1000.0 / 60 * 0.001;
                }
                else
                {
                    await Task.Delay(1);
                    DeltaTime = ++pastTime * 0.001;
                }
                frameCount++;
                if (stopwatch.ElapsedMilliseconds - startTime >= 1000)
                {
                    startTime = stopwatch.ElapsedMilliseconds;
                    FPS = frameCount;
                    frameCount = 0;
                }
                previourTime = stopwatch.ElapsedMilliseconds;
            }
        }

        public void Start()
        {
            List<GameObject> gameObjects = Children.OfType<GameObject>().ToList();

            foreach (GameObject gameObject in gameObjects)
                gameObject.Scene = this;

            for(int i = 0; i< gameObjects.Count; i++)
                if (gameObjects[i].Scripts.Count > 0)
                    foreach(Script script in gameObjects[i].Scripts)
                        script.Start();
        }

        public void Update()
        {
            List<GameObject> CurChildren = Children.OfType<GameObject>().ToList();
            foreach (GameObject gameObject in CurChildren)
                if (gameObject.Scripts.Count > 0)
                    foreach (Script script in gameObject.Scripts)
                    {
                        if (IsEndGame) return;
                            script.Update();
                    }

            ApplyGravity();
        }

        public void ApplyGravity()
        {
            List<GameObject> shapes = Children.OfType<GameObject>().Where(x => x.Collider != null).ToList();

            foreach (GameObject shape in shapes)
            {
                if (!shape.IsGravity || shape.IsFallingNow) continue;

                foreach (GameObject otherShape in shapes)
                    if (shape != otherShape && shape.Collider.Bounds.IntersectsWith(otherShape.Collider.Bounds))
                        return;

                Falling(shape);
            }
        }

        public async void Falling(GameObject shape)
        {
            shape.IsFallingNow = true;
            while (true)
            {
                List<GameObject> shapes = Children.OfType<GameObject>().Where(x => x.Collider != null).ToList();

                int fallSpeed = 2;
                while (fallSpeed > 0)
                {
                    foreach (GameObject otherShape in shapes)
                        if (shape != otherShape && shape.Collider.Bounds.IntersectsWith(otherShape.Collider.Bounds) &&
                            shape.Position.Y - shape.Size.Y / 2.0  + 0.01 >= otherShape.Position.Y + otherShape.Size.Y / 2.0)
                        {
                            shape.IsFallingNow = false;
                            return;
                        }

                    Point3D fal = shape.Position;
                    fal.Y -= 0.01;
                    shape.Dispatcher.Invoke(() => shape.Position = fal);
                    fallSpeed --;
                    if (isEndGame) return;
                }
                await Task.Delay(1);
            }
        }

        public void Instantiate(GameObject gameObject) => Children.Insert(0, gameObject);
        public void EndGame()
        {
            GameObject player = Children.OfType<GameObject>().First(x => x.Name == "Player");

            if (player.Position.Z > User.Score)
                User.Score = (int)Math.Floor(player.Position.Z);

            IsEndGame = true;
        }
        

    }
}
