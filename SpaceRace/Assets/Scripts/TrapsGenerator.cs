﻿using SpaceRace.Assets.Prefabs;
using SpaceRace.BaseClasses;
using System;
using System.Collections.Generic;

namespace SpaceRace.Assets.Scripts
{
    public class TrapsGenerator : Script
    {
        private readonly List<Trap> Traps = new(30);
        private double oldPlayerPositionZ = 0;

        public override void Start()
        {
            GenerateTraps();
        }
        public override void Update()
        {
            if (GameObject.Position.Z > oldPlayerPositionZ + 10)
                GenerateTraps();

            ChangeTrapsColor();
        }
        private void GenerateTraps()
        {
            oldPlayerPositionZ += 25;
            Random rand = new();
            int countTraps = rand.Next(3, 8);

            while (countTraps > 0)
            {
                double positionX = 0.5;
                bool isGood = false;
                while (!isGood && Traps.Count != 0)
                {
                    isGood = true;
                    positionX = rand.Next(5, 15) - 9.5;
                    foreach (Trap trap in Traps)
                        if (trap.Position.X == positionX && trap.Position.Z == oldPlayerPositionZ + 25)
                        {
                            isGood = false;
                            break;
                        }
                }
                if (Traps.Count == 27)
                {
                    GameObject.Scene.Children.Remove(Traps[0]);
                    Traps.RemoveAt(0);
                }
                Trap newTrap = new(new(positionX, 0.5, oldPlayerPositionZ + 25));
                Traps.Add(newTrap);
                GameObject.Scene.Instantiate(newTrap);
                countTraps--;
            }
        }

        private void ChangeTrapsColor()
        {
            foreach (Trap trap in Traps)
            {
                trap.Material = ColorChanger.ColorsMaterial;

                if (GameObject.Collider.Bounds.IntersectsWith(trap.Collider.Bounds))
                {
                    GameObject.Scene.EndGame();
                    return;
                }
            }
        }
    }
}
