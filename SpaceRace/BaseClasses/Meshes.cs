﻿using System;
using System.Windows.Media;
using System.Windows.Media.Media3D;

namespace SpaceRace.BaseClasses
{    
    public static class Meshes
    {
        /// <summary>
        /// Create and returns quad mesh
        /// </summary>
        /// <returns></returns>
        public static MeshGeometry3D Quad()
            => new()
            {
                Positions = new()
                {
                    new Point3D(0 - 0.5, 0, 0 - 0.5),
                    new Point3D(0 - 0.5, 0, 0 + 0.5),
                    new Point3D(0 + 0.5, 0, 0 + 0.5),
                    new Point3D(0 + 0.5, 0, 0 - 0.5),
                },
                TriangleIndices = new()
                {
                    0, 1, 3,
                    3, 1, 2,
                },
                TextureCoordinates = new()
                {
                    new(0,0),
                    new(0,1),
                    new(1,1),
                    new(1,0),
                }
            };
      
        /// <summary>
        /// Create and returns cube mesh
        /// </summary>
        /// <returns></returns>
        public static MeshGeometry3D Cube() 
            => new()
            {
                Positions = new()
                {
                    // Bottom face
                    new Point3D(0 - 0.5, 0 - 0.5, 0 - 0.5),
                    new Point3D(0 + 0.5, 0 - 0.5, 0 - 0.5),
                    new Point3D(0 + 0.5, 0 - 0.5, 0 + 0.5),
                    new Point3D(0 - 0.5, 0 - 0.5, 0 + 0.5),

                    // Top face
                    new Point3D(0 - 0.5, 0 + 0.5, 0 - 0.5),
                    new Point3D(0 + 0.5, 0 + 0.5, 0 - 0.5),
                    new Point3D(0 + 0.5, 0 + 0.5, 0 + 0.5),
                    new Point3D(0 - 0.5, 0 + 0.5, 0 + 0.5),
                },
                TriangleIndices = new()
                {
                    // Front face
                    0, 4, 1,
                    1, 4, 5,

                    // Top face
                    4, 6, 5,
                    6, 4, 7,

                    // Back face
                    2, 6, 3,
                    3, 6, 7,

                    // Bottom face
                    0, 1, 2,
                    2, 3, 0,

                    // Left face
                    0, 3, 4,
                    4, 3, 7,

                    // Right face
                    1, 5, 2,
                    2, 5, 6,
                }
            };


        #region Sphere mesh
        private static readonly int countVerticalSegments = 16;
        private static readonly int countHorizontalSegments = 16;
        public static MeshGeometry3D Sphere()
            => new()
            {
                Positions = GetSphereVertices(countHorizontalSegments, countVerticalSegments),
                TriangleIndices = GetSphereIndices(countHorizontalSegments, countVerticalSegments),
                TextureCoordinates = GetSphereTextureCoordinates(countHorizontalSegments, countVerticalSegments)
            };
        private static Point3DCollection GetSphereVertices(int numPhi, int numTheta)
        {
            Point3DCollection vertices = new(numPhi * numTheta);
            for (int phi = 0; phi <= numPhi; phi++)
            {
                double phiRadians = phi * Math.PI / numPhi;
                for (int theta = 0; theta <= numTheta; theta++)
                {
                    double thetaRadians = theta * 2 * Math.PI / numTheta;
                    vertices.Add(new(
                        0 + 0.5 * Math.Cos(phiRadians),
                        0 + 0.5 * Math.Sin(phiRadians) * Math.Cos(thetaRadians),
                        0 + 0.5 * Math.Sin(phiRadians) * Math.Sin(thetaRadians)));
                }
            }

            return vertices;
        }
        private static Int32Collection GetSphereIndices(int numPhi, int numTheta)
        {
            Int32Collection indices = new(numPhi * numTheta);
            for (int phi = 0; phi < numPhi; phi++)
                for (int theta = 0; theta < numTheta; theta++)
                {
                    int first = phi * (numTheta + 1) + theta;
                    int second = first + numTheta + 1;

                    indices.Add(first);
                    indices.Add(second);
                    indices.Add(first + 1);

                    indices.Add(second);
                    indices.Add(second + 1);
                    indices.Add(first + 1);
                }

            return indices;
        }
        private static PointCollection GetSphereTextureCoordinates(int numSegments, int numSlices)
        {
            PointCollection textureCoordinates = new((numSegments) * (numSlices) + 2);

            // Calculate texture coordinates for the body of the sphere
            for (int i = 0; i <= numSegments; i++)
                for (int j = 0; j <= numSlices; j++)
                {
                    float u = (float)j / numSegments;
                    float v = (float)i / numSlices;
                    textureCoordinates.Add(new(u, v));
                }


            // Calculate texture coordinates for the north pole
            textureCoordinates.Add(new(0.5f, 0.0f));

            // Calculate texture coordinates for the south pole
            textureCoordinates.Add(new(0.5f, 0.8f));

            return textureCoordinates;
        }
        #endregion
    }
}
