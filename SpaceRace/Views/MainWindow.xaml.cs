﻿using SpaceRace.Assets.Scenes;
using SpaceRace.BaseClasses;
using SpaceRace.Views;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Navigation;

namespace SpaceRace
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public static readonly DependencyProperty CurPageProperty = DependencyProperty.Register(
            "CurPage", typeof(Page), typeof(MainWindow));
        public Page CurPage
        {
            get => (Page)GetValue(CurPageProperty);
            set => SetValue(CurPageProperty, value);
        }
        private Page VerificationPage;
        private Page GamePage;


        public MainWindow()
        {
            DataContext = this;
            InitializeComponent();
            Loaded += (s, e) =>
            {
                VerificationPage = new VerificationView(this);
                CurPage = VerificationPage;
            };
            
        }
        public void OpenMenu() => CurPage = VerificationPage;
        public void StartGame(string userName)
        {
            GamePage = new GameView(this, userName);
            CurPage = GamePage;
        }


    }
}
