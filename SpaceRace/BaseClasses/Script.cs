﻿
namespace SpaceRace.BaseClasses
{
    public class Script
    {
        public GameObject GameObject;
        public virtual void Start() { }
        public virtual void Update() { }
    }
}
